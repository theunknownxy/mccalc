#include "dotrenderer.hpp"
#include <core/recipemanager.hpp>

#include <sstream>

DotRenderer::DotRenderer(RecipeManager &recipemgr)
    : Renderer(recipemgr)
{}

std::string DotRenderer::render(TargetList &targets)
{
    std::stringstream ss;
    // Write the header
    ss << "digraph{" << "node[shape=box];";
    // Write Items
    for (const auto &item : this->recipemgr_.getRecipes())
    {
        if (item.second.Requests < 1)
            continue;

        ss << item.first << "[label=\"" << item.second.Requests << "x " << item.second.getName() << "\"];";
        // Check if current item is a target
        if (targets.getTargets().count(item.first))
        {
            ss << item.first << "[style=bold];";
        }
        // Add edges to dependencies
        for (const auto &dependency : item.second.Input)
        {
            ss << item.first << "->" << dependency.Stack.ID << "[fontsize=10,label=\"" << dependency.Request << "x\"];";
        }
    }

    // Write end
    ss << "}";

    return ss.str();
}
