#ifndef DOTRENDERER_HPP
#define DOTRENDERER_HPP

#include "core/recipemanager.hpp"
#include "render/renderer.hpp"

class DotRenderer : public Renderer
{
public:
    DotRenderer(RecipeManager &recipemgr);
    virtual std::string render(TargetList &targets) override;
};

#endif // DOTRENDERER_HPP
