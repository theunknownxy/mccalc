#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <string>

#include "core/recipe.hpp"
#include "core/targetlist.hpp"
#include "core/recipemanager.hpp"

class Renderer
{
public:
    Renderer(RecipeManager &recipemgr_);
    virtual ~Renderer();
    virtual std::string render(TargetList &targets) = 0;
protected:
    RecipeManager &recipemgr_;
};

#endif // RENDERER_HPP
