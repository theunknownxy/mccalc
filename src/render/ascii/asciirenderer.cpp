#include "asciirenderer.hpp"

#include <iostream>
#include <sstream>
#include <cmath>
#include <iomanip>

#include "core/recipemanager.hpp"

void formatItem(std::stringstream &ss, const Recipe &recipe, int count)
{
    if (count > 0)
    {
        ss << std::setw(20) << recipe.Name << ": ";
        ss << std::left << std::setw(4) << count;
        if (count > 64)
        {
            int stacks = count / 64;
            int items = count % 64;
            ss << "(" << std::setw(2) << stacks << " Stacks";
            if (items != 0)
            {
                ss << ", " << std::setw(2) << count % 64 << " Items)";
            }
            else
            {
                ss << ")";
            }
        }
        ss << "\n";
        ss << std::internal;
    }
}

void formatItem(std::stringstream &ss, const Recipe &recipe)
{
    formatItem(ss, recipe, recipe.Requests);
}

ASCIIRenderer::ASCIIRenderer(RecipeManager &recipemgr)
    : Renderer(recipemgr), renderOnlyRaw_(true)
{}

ASCIIRenderer::ASCIIRenderer(RecipeManager &recipemgr, bool renderOnlyRaw)
    : Renderer(recipemgr), renderOnlyRaw_(renderOnlyRaw)
{}

std::string ASCIIRenderer::render(TargetList &targets)
{
    std::stringstream result;

    for (const auto &target : targets.getTargets())
    {
        Recipe &rec = this->recipemgr_.getRecipe(target.first);
        formatItem(result, rec, target.second);
    }

    result << "... require ...\n";

    for (const auto &recipe : this->recipemgr_.getRecipes())
    {
        // Skip non-raw recipes if requested.
        if (this->renderOnlyRaw_ && !recipe.second.Raw)
            continue;

        // Only render items, which aren't targets.
        if (targets.getTargets().count(recipe.second.Output.ID) == 0)
        {
            formatItem(result, recipe.second);
        }
    }

    return result.str();
}
