#ifndef ASCIIRENDERER_HPP
#define ASCIIRENDERER_HPP

#include "core/recipemanager.hpp"
#include "render/renderer.hpp"

class ASCIIRenderer : public Renderer
{
public:
    ASCIIRenderer(RecipeManager &recipemgr);
    ASCIIRenderer(RecipeManager &recipemgr, bool renderOnlyRaw);
    virtual std::string render(TargetList &targets) override;

private:
    bool renderOnlyRaw_;
};

#endif // ASCIIRENDERER_HPP
