#ifndef PARSER_HPP
#define PARSER_HPP

#include "string"

#include "core/recipemanager.hpp"

class Parser
{
public:
    virtual void registerRecipes(std::string filename, RecipeManager &mgr) = 0;
};

#endif // PARSER_HPP
