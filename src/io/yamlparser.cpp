#include "yamlparser.hpp"

#include <iostream>
#include <map>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/xpressive/xpressive.hpp>
#include <boost/lexical_cast.hpp>
#include <yaml-cpp/yaml.h>

void YamlParser::registerRecipes(std::string filename, RecipeManager &mgr)
{
    using namespace boost::xpressive;

    YAML::Node recipesFile = YAML::LoadFile(filename);
    for (const auto &entry : recipesFile)
    {
            Recipe recipe;
            for (const auto &field : entry)
            {
                std::string fieldname = field.first.as<std::string>();
                if (fieldname == "name")
                    recipe.Name = field.second.as<std::string>();
                else if (fieldname == "id")
                    recipe.Output.ID = field.second.as<int>();
                else if (fieldname == "raw")
                    recipe.Raw = field.second.as<bool>();
                else if (fieldname == "output")
                    recipe.Output.Count = field.second.as<int>();
                else if (fieldname == "recipe")
                {
                    recipe.Raw = false; // The recipe isn't raw

                    std::string rawstring = field.second.as<std::string>();
                    std::forward_list<std::string> tokens;
                    std::map<int, int> inputItems; // map[id]=occurences
                    static sregex itemre = !((s1 = +_d) >> (as_xpr(":") | as_xpr("x"))) >> (s2 = (+_d) | "_"); // regex: (?:(\d+)(:|x))?(\d+)

                    boost::split(tokens, rawstring, boost::is_any_of(" "));
                    for (const std::string &token : tokens)
                    {
                        smatch what;
                        if (regex_match(token, what, itemre))
                        {
                            int count = 1;
                            int id;

                            if (what[1])
                            {
                                try
                                {
                                    count = boost::lexical_cast<int>(what[1]);
                                }
                                catch(boost::bad_lexical_cast &e)
                                {
                                    std::cerr << "Can't parse count: " << e.what();
                                }
                            }

                            // Check whether the field is empty
                            if (what[2] != "_")
                            {
                                try
                                {
                                    id = boost::lexical_cast<int>(what[2]);
                                    inputItems[id] += count;
                                    recipe.InputRecipe.push_back(token);
                                }
                                catch(boost::bad_lexical_cast &e)
                                {
                                    std::cerr << "Can't parse ID: " << e.what();
                                }
                            }
                            else
                            {
                                recipe.InputRecipe.push_back("0");
                            }
                        }
                        else
                        {
                            std::cerr << "Unrecognized ingredient '" << token << "'";
                        }
                    }

                    for (const auto &item : inputItems)
                    {
                        recipe.Input.push_front(Relation(item.first, item.second));
                    }
                }
            }
            // Finally add the recipe to the Recipe Manager
            mgr.registerRecipe(std::move(recipe));
    }

}
