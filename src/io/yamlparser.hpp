#ifndef YAMLPARSER_HPP
#define YAMLPARSER_HPP

#include <string>

#include "parser.hpp"

class YamlParser : public Parser
{
public:
    virtual void registerRecipes(std::string filename, RecipeManager &mgr) override;
};

#endif // YAMLPARSER_HPP
