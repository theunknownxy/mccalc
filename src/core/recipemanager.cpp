#include "recipemanager.hpp"

#include <stdexcept>

RecipeManager::RecipeManager()
{}

Recipe & RecipeManager::getRecipe(int id)
{
    try
    {
        return this->m_recipes.at(id);
    }
    catch(std::out_of_range &e)
    {
        throw RecipeNotFoundException();
    }
}

std::map<int, Recipe> RecipeManager::getRecipes()
{
    return this->m_recipes;
}

void RecipeManager::registerRecipe(Recipe && recipe)
{
    this->m_recipes.insert(std::pair<int, Recipe>(recipe.Output.ID, recipe));
}
