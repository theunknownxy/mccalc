#include "recipe.hpp"

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <boost/lexical_cast.hpp>

#include "recipemanager.hpp"

Recipe::Recipe()
    : Raw(true)
{}

Recipe::Recipe(ItemStack output)
    : Output(output), Raw(true)
{}

Recipe::Recipe(ItemStack output, std::forward_list<Relation> input)
    : Output(output), Input(input)
{}

Recipe::Recipe(ItemStack output, std::initializer_list<Relation> input)
    : Output(output), Input(input)
{}

std::string Recipe::getName() const
{
    if (this->Name.empty())
        return std::string("X") + boost::lexical_cast<std::string>(this->Output.ID);
    else
        return this->Name;
}

void Recipe::requestItems(int count)
{
    this->Used = true;
    this->Requests += count;
}
