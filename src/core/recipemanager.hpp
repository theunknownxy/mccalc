#ifndef RECIPEMANAGER_HPP
#define RECIPEMANAGER_HPP

#include <map>

#include "recipe.hpp"
#include "itemstack.hpp"

class RecipeManager
{
public:
    RecipeManager();
    Recipe& getRecipe(int id);
    std::map<int, Recipe> getRecipes();
    void registerRecipe(Recipe && recipe);

private:
    std::map<int, Recipe> m_recipes; // ID -> Recipe
};

class RecipeNotFoundException : public std::exception
{};

#endif /* RECIPEMANAGER_HPP */
