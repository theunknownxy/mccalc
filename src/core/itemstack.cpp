#include "itemstack.hpp"

ItemStack::ItemStack()
    : ID(0), Count(1)
{}

ItemStack::ItemStack(int id, int count)
    : ID(id), Count(count)
{}
