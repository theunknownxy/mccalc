include_directories(${Boost_INCLUDE_DIRS})
set(LIBMCCALCCORE_SOURCE targetlist.cpp targetlist.hpp relation.hpp solver.cpp solver.hpp recipe.cpp recipe.hpp itemstack.cpp itemstack.hpp recipemanager.cpp recipemanager.hpp)
add_library (mcCalcCore STATIC ${LIBMCCALCCORE_SOURCE})
