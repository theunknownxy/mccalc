#ifndef TARGETLIST_HPP
#define TARGETLIST_HPP

#include <map>

#include "core/recipemanager.hpp"

class TargetList
{
public:
    TargetList(RecipeManager &recipemgr);
    void modifyTarget(int id, int count);
    const std::map<int, int> getTargets();
    std::map<int, int> targets_; // ID -> Count
protected:
    RecipeManager &recipemgr_;
};

#endif // TARGETLIST_HPP
