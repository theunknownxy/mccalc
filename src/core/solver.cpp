#include "solver.hpp"

#include <cmath>

#include "recipemanager.hpp"

Solver::Solver(RecipeManager &recipemgr, TargetList &targets)
    : targets_(targets), recipemgr_(recipemgr)
{}

void Solver::solve()
{
    for (auto target : this->targets_.getTargets())
    {
        updateRecipeDepends(recipemgr_.getRecipe(target.first));
    }
}

static int cycles(Recipe &recipe)
{
    return (int)ceil((double)recipe.Requests / recipe.Output.Count);
}

void Solver::updateRecipeDepends(Recipe &recipe)
{
    for (auto &relation : recipe.Input)
    {
        Recipe &input = this->recipemgr_.getRecipe(relation.Stack.ID);
        int currReq = relation.Stack.Count * cycles(recipe); // How much we need now?
        int requestDiff = currReq - relation.Request;  // Difference to the previous request

        // Only recurse if there was a change
        if (requestDiff != 0)
        {
            input.requestItems(requestDiff); // Only request the difference between this and the previous update
            relation.Request = currReq; // Update the previous request

            updateRecipeDepends(input); // Recurse and update the dependencies of the dependency.
        }
    }
}
