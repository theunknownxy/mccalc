#include "targetlist.hpp"

TargetList::TargetList(RecipeManager &recipemgr)
    : recipemgr_(recipemgr)
{}

void TargetList::modifyTarget(int id, int count)
{
    this->recipemgr_.getRecipe(id).requestItems(count);
    this->targets_[id] += count;
}

const std::map<int, int> TargetList::getTargets()
{
    return this->targets_;
}
