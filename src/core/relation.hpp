#ifndef RELATION_HPP
#define RELATION_HPP

#include <initializer_list>

#include "itemstack.hpp"

struct Relation
{
    Relation(int id, int count = 1)
        : Stack(id, count)
    {}
    ItemStack Stack;
    int Request = 0;
};

#endif // RELATION_HPP
