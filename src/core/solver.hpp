#ifndef SOLVER_HPP
#define SOLVER_HPP

#include <list>
#include <map>

#include "recipe.hpp"
#include "targetlist.hpp"

class Solver
{
public:
    Solver(RecipeManager &recipemgr, TargetList &targets);
    void solve();
private:
    void updateRecipeDepends(Recipe &recipe);
    TargetList &targets_;
    RecipeManager &recipemgr_;
};

#endif // SOLVER_HPP
