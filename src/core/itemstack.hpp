#ifndef ITEMSTACK_HPP
#define ITEMSTACK_HPP

class ItemStack
{
public:
    ItemStack();
    ItemStack(int id, int count = 1);
    int Count;
    int ID;
};

#endif /* ITEMSTACK_HPP */
