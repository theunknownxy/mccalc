#ifndef RECIPE_HPP
#define RECIPE_HPP

#include <string>
#include <forward_list>
#include <list>

#include "itemstack.hpp"
#include "relation.hpp"

class Recipe
{
public:
    Recipe();
    Recipe(ItemStack output);
    Recipe(ItemStack output, std::forward_list<Relation> input);
    Recipe(ItemStack output, std::initializer_list<Relation> input);
    std::string getName() const;
    void requestItems(int count);

    std::string Name;
    std::forward_list<Relation> Input;
    ItemStack Output;
    bool Used = false;
    bool Raw = false;
    int Requests = 0;

    std::list<std::string> InputRecipe;
};

#endif /* RECIPE_HPP */
