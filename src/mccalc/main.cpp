#include <iostream>
#include <fstream>

#include <tclap/CmdLine.h>
#include <boost/xpressive/xpressive.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "core/recipemanager.hpp"
#include "core/targetlist.hpp"
#include "core/solver.hpp"
#include "io/yamlparser.hpp"
#include "render/renderer.hpp"
#include "render/dot/dotrenderer.hpp"
#include "render/ascii/asciirenderer.hpp"

using namespace std;

#define VERSION "0.1-git"

bool file_exists(const string &filename)
{
    ifstream file(filename);
    return file;
}

// pair<ID, Count>
pair<int, int> parseTarget(std::string target)
{
    using namespace boost::xpressive;
    static sregex itemre = !((s1 = !as_xpr("-") >> +_d) >> as_xpr(":")) >> (s2 = (+_d)); // Regex: (?:(-\d+):)?(\d+)

    smatch what;
    if (regex_match(target, what, itemre))
    {
            int id;
            int count;
            // Optional Count(default = 1)
            if (what[1])
            {
                try
                {
                    count = boost::lexical_cast<int>(what[1]);
                }
                catch(boost::bad_lexical_cast &e)
                {
                    throw string("parseTarget: Invalid count '" + what[1] + "'");
                }
            }
            else
            {
                count = 1;
            }

            // ID
            try
            {
                id = boost::lexical_cast<int>(what[2]);
            }
            catch(boost::bad_lexical_cast &e)
            {
                throw string("parseTarget: Invalid id '" + what[2] + "'");
            }

            return pair<int, int>(id, count);
    }
    else
    {
        throw string("Can't parse target '" + target + "'");
    }
}

int main(int argc, char **argv)
{
    RecipeManager recipemgr;
    TargetList targetlist(recipemgr);
    Solver solver(recipemgr, targetlist);
    YamlParser parser;

    try
    {
        TCLAP::CmdLine cmd("Minecraft resource calculator", ' ', VERSION);

        TCLAP::MultiArg<string> recipeFilesArg("r", "recipeFile", "The Recipe Description file to use", true, "file");
        cmd.add(recipeFilesArg);

        TCLAP::UnlabeledMultiArg<string> targetsArg("Targets", "The Recipes to calculate in the format '[<count>:]<id>'", true, "recipes");
        cmd.add(targetsArg);

        TCLAP::SwitchArg showAllInputsArg("a", "all-inputs", "Whether print also non raw items. Only the text output format uses this flag.");
        cmd.add(showAllInputsArg);

        vector<string> outputFormats = {"text", "dot"};
        TCLAP::ValuesConstraint<string> outputFormatsConstraint(outputFormats);
        TCLAP::ValueArg<string> outputFormatArg("o", "output-format", "Which output format to use", false, "text", &outputFormatsConstraint);
        cmd.add(outputFormatArg);

        cmd.parse(argc, argv);

        vector<string> recipeFiles = recipeFilesArg.getValue();
        vector<string> targets = targetsArg.getValue();
        string outputFormat = outputFormatArg.getValue();
        bool showAllInputs = showAllInputsArg.getValue();

        // Register all recipes
        for (const auto &filename : recipeFiles)
        {
            if (!file_exists(filename))
            {
                throw string("File '" + filename + "' doesn't exist");
            }

            if (boost::algorithm::ends_with(filename, ".yml"))
            {
                parser.registerRecipes(filename, recipemgr);
            }
            else
            {
                throw string("Unknown filetype of recipe file '" + filename + "'!");
            }
        }

        // Add targets
        for (const auto &target : targets)
        {
            pair<int, int> t = parseTarget(target);
            try
            {
                targetlist.modifyTarget(t.first, t.second);
            }
            catch(RecipeNotFoundException &e)
            {
                throw string("Can't find recipe with id='" + boost::lexical_cast<string>(t.first) + "'");
            }
        }
        // Calculate
        solver.solve();

        // Output
        Renderer *renderer;
        if (outputFormat == "text")
        {
            // Inversion :)
            renderer = new ASCIIRenderer(recipemgr, !showAllInputs);
        }
        else if (outputFormat == "dot")
        {
            renderer = new DotRenderer(recipemgr);
        }
        else
        {
            // This *should* be dead code, because TCLAP already checks the output format.
            throw string("Unknown output format '" + outputFormat + "'");
        }
        cout << renderer->render(targetlist);
        delete renderer;
    }
    catch(TCLAP::ArgException &e)
    {
        cerr << "Argument Error: " << e.what() << " for arg " << e.argId();
    }
    catch(string &e)
    {
        cerr << "Error: " << e << endl;
    }
    return 0;
}
