#include <random>

#include <gtest/gtest.h>

#include "core/recipe.hpp"
#include "core/recipemanager.hpp"
#include "core/solver.hpp"
#include "core/targetlist.hpp"

#define ITEMA 1
#define ITEMB 2
#define ITEMC 3
#define ITEMD 4
#define ITEME 5
#define ITEMF 6
#define ITEMG 7
#define ITEMH 8

class SolverTest : public testing::Test
{
protected:
    virtual void SetUp()
    {
        recipemgr = new RecipeManager();
        targetList = new TargetList(*recipemgr);
        solver = new Solver(*recipemgr, *targetList);

        // Base recipes
        recipemgr->registerRecipe(Recipe {{ITEMA}});
        recipemgr->registerRecipe(Recipe {{ITEMB}});
        recipemgr->registerRecipe(Recipe {{ITEMC}});

        recipemgr->registerRecipe(Recipe {{ITEMH}, {{ITEMA}, {ITEMB}}});

        recipemgr->registerRecipe(Recipe {{ITEMD, 4}, {{ITEMA, 3}, {ITEMC, 1}}});

        recipemgr->registerRecipe(Recipe {{ITEME}, {{ITEMD, 3}}});
    }

    virtual void TearDown()
    {
        delete this->solver;
        delete this->targetList;
        delete this->recipemgr;
    }

    RecipeManager *recipemgr;
    TargetList *targetList;
    Solver *solver;
};

TEST_F(SolverTest, SimpleRequest)
{
    targetList->modifyTarget(ITEMH, 24);
    solver->solve();
    EXPECT_EQ(recipemgr->getRecipe(ITEMH).Requests, 24);
    EXPECT_EQ(recipemgr->getRecipe(ITEMA).Requests, 24);
    EXPECT_EQ(recipemgr->getRecipe(ITEMB).Requests, 24);
}

TEST_F(SolverTest, MultipleInputOutput)
{
    targetList->modifyTarget(ITEME, 1);
    solver->solve();
    EXPECT_EQ(recipemgr->getRecipe(ITEME).Requests, 1);
    EXPECT_EQ(recipemgr->getRecipe(ITEMD).Requests, 3);
    EXPECT_EQ(recipemgr->getRecipe(ITEMA).Requests, 3);
    EXPECT_EQ(recipemgr->getRecipe(ITEMC).Requests, 1);
}

TEST_F(SolverTest, Undo)
{
    targetList->modifyTarget(ITEME, 1);
    solver->solve();
    EXPECT_EQ(recipemgr->getRecipe(ITEME).Requests, 1);
    EXPECT_EQ(recipemgr->getRecipe(ITEMD).Requests, 3);
    EXPECT_EQ(recipemgr->getRecipe(ITEMA).Requests, 3);
    EXPECT_EQ(recipemgr->getRecipe(ITEMC).Requests, 1);
    targetList->modifyTarget(ITEME, -1);
    solver->solve();
    EXPECT_EQ(recipemgr->getRecipe(ITEME).Requests, 0);
    EXPECT_EQ(recipemgr->getRecipe(ITEMD).Requests, 0);
    EXPECT_EQ(recipemgr->getRecipe(ITEMA).Requests, 0);
    EXPECT_EQ(recipemgr->getRecipe(ITEMC).Requests, 0);
}
