#include <gtest/gtest.h>

#include "core/recipe.hpp"
#include "core/recipemanager.hpp"

class RecipeManagerTest : public testing::Test 
{
protected:
    virtual void SetUp()
    {
        this->recipemgr = new RecipeManager();
    }

    virtual void TearDown()
    {
        delete this->recipemgr;
    }
    RecipeManager *recipemgr;
};

TEST_F(RecipeManagerTest, Empty)
{
    EXPECT_EQ(recipemgr->getRecipes().size(), 0);
}

TEST_F(RecipeManagerTest, GetByID)
{
    recipemgr->registerRecipe(Recipe{{42}});

    EXPECT_EQ(recipemgr->getRecipes().size(), 1);
    EXPECT_NO_THROW({
        Recipe &recipe = recipemgr->getRecipe(42);
        EXPECT_EQ(recipe.Output.ID, 42);
    });
}
