#include <gtest/gtest.h>

#include "core/recipemanager.hpp"
#include "core/recipe.hpp"
#include "io/yamlparser.hpp"

class YamlParserTest : public testing::Test
{
    virtual void SetUp()
    {
        recipemgr = new RecipeManager();
        yamlparser = new YamlParser();
    }

    virtual void TearDown()
    {
        delete recipemgr;
        delete yamlparser;
    }

    RecipeManager *recipemgr;
    YamlParser *yamlparser;
};

TEST_F(YamlParserTest, SimpleRecipe)
{
    // TODO: Write an unittests for YamlParser
}
